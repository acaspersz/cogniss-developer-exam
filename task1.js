const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputFile = "input2.json";
const inputURL = "https://jsonplaceholder.typicode.com/posts/1";
const outputFile = "output2.json";

var output = {}
console.log("loading input json from input2 file", inputURL);

got(inputURL).then(response => {
  // console.log("loaded input file content", response.body);
  // output.post = response.body;
  console.log("loading input file", inputFile);
  jsonfile.readFile(inputFile, function(err, body){
    console.log("loaded input file content", body);
    output.document = body;
    console.log("name, reverse it, append 5 random characters plus @gmail.com");
    // output.email = [];
    for (var i=0; i<20; i++) {
      output.document.split('').reverse().join('')
      output.document.push(randomstring.generate(5));
      output.document.concat('@gmail.com');
    }
    console.log("generated new email", output.email);
    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
      console.log("All done!");
    });
  });
}).catch(err => {
  console.log(err)
})
